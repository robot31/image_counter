# Image Counter ROS Package

This ROS package provides nodes for counting images, saving prime-numbered images, and displaying images in a window.

## Prerequisites

- ROS (Robot Operating System) installed on your system.
- Python 2.7 or later (for ROS Kinetic) or Python 3 (for ROS Melodic and later).

## Installation

Clone this repository to your ROS workspace's `src` directory:

```bash
cd ~/catkin_ws/src
```
```bash
git clone https://gitlab.com/robot31/image_counter.git
```
Build your ROS workspace:

```bash
cd ~/catkin_ws
```
```bash
catkin_make
```
**Usage**
1. Launch the ROS nodes:
```bash
roslaunch image_counter image_processing.launch
```
This will start the "Image Counter," "Image Path Saver," and "Image Publisher" nodes.

2. To reset the image counter, use the following command:
```bash
rostopic pub /reset std_msgs/String "data: 'reset'"
```

This will reset the image counter and empty the list of saved image paths.

3. View the saved image paths in the image_paths.txt file in the user's home directory.

**Customization**

You can modify the behavior of the nodes by editing the Python scripts located in the scripts directory. Feel free to adapt the code to suit your specific requirements.


**Contributing**

Contributions are welcome! Please create a merge request for any improvements or fixes.
License

This project is licensed under the MIT License.
