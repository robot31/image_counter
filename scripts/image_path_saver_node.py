#!/usr/bin/env python

import rospy
import os 
from std_msgs.msg import String

class ImagePathSaverNode:
    def __init__(self):
        self.img_paths = []
        self.img_path_sub = rospy.Subscriber('/img_path', String, self.img_path_callback)
        self.reset_sub = rospy.Subscriber('/reset', String, self.reset_callback)

    def img_path_callback(self, img_path_msg):
        img_path = img_path_msg.data
        self.img_paths.append(img_path)
        self.save_paths_to_file()

    def reset_callback(self, reset_msg):
        self.img_paths = []
        self.save_paths_to_file()

    def save_paths_to_file(self):
        file_path = rospy.get_param('~file_path', '~/image_paths.txt')
        with open(os.path.expanduser(file_path), 'w') as f:
            for img_path in self.img_paths:
                f.write(img_path + '\n')

def main():
    rospy.init_node('image_path_saver_node', anonymous=True)
    image_path_saver = ImagePathSaverNode()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo("Image Path Saver: Shutting down.")

if __name__ == '__main__':
    main()
