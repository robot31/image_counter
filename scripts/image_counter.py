#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge
import cv2
import os
import math

class ImageCounterNode:
    def __init__(self):
        self.counter = 0
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber('/cam_img', Image, self.image_callback)
        self.reset_sub = rospy.Subscriber('/reset', String, self.reset_callback)
        self.img_path_pub = rospy.Publisher('/img_path', String, queue_size=10)

    def is_prime(self, num):
        if num <= 1:
            return False
        if num <= 3:
            return True
        if num % 2 == 0 or num % 3 == 0:
            return False
        sqrt_num = int(math.sqrt(num))
        for i in range(5, sqrt_num + 1, 6):
            if num % i == 0 or num % (i + 2) == 0:
                return False
        return True

    def save_image(self, image_msg):
        img = self.bridge.imgmsg_to_cv2(image_msg, "bgr8")
        file_name = f"image_{self.counter}.jpg"
        file_path = os.path.join(os.path.expanduser('~'), 'image_counter_images', file_name)
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        cv2.imwrite(file_path, img)
        return file_path

    def display_image(self, image_msg):
        img = self.bridge.imgmsg_to_cv2(image_msg, "bgr8")
        cv2.imshow("Current Image", img)
        cv2.waitKey(1)  # Wait for a short time to allow the window to refresh

    def image_callback(self, image_msg):
        self.counter += 1
        if self.is_prime(self.counter):
            img_path = self.save_image(image_msg)
            rospy.loginfo(f"Image Counter: Saved image {img_path}")
            self.img_path_pub.publish(img_path)
        self.display_image(image_msg)  # Call the display_image function

    def reset_callback(self, reset_msg):
        self.counter = 0
        rospy.loginfo("Image Counter: Counter reset to 0.")

    def on_shutdown(self):
        cv2.destroyAllWindows()

def main():
    rospy.init_node('image_counter_node', anonymous=True)
    image_counter = ImageCounterNode()
    rospy.on_shutdown(image_counter.on_shutdown)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo("Image Counter: Shutting down.")

if __name__ == '__main__':
    main()
