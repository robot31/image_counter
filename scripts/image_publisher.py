#!/usr/bin/env python

import rospy
import cv2
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

def main():
    rospy.init_node('image_publisher', anonymous=True)
    rate = rospy.Rate(10)  # Publish rate (10 Hz)

    cap = cv2.VideoCapture(0)  # Initialize the webcam (default camera index)

    bridge = CvBridge()
    pub = rospy.Publisher('/cam_img', Image, queue_size=10)

    while not rospy.is_shutdown():
        ret, frame = cap.read()  # Read a frame from the webcam
        if ret:
            ros_image = bridge.cv2_to_imgmsg(frame, "bgr8")  # Convert OpenCV image to ROS image message
            pub.publish(ros_image)  # Publish the ROS image message to the topic
        rate.sleep()

    cap.release()  # Release the webcam
    cv2.destroyAllWindows()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
